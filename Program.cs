﻿using Lesson6.Models;

internal class Program
{
    private static void Main(string[] args)
    {
        Phone xiaomi = new ("+375(29)111-22-33","Xiaomi 10T", 133.3);
        Phone iphone = new ("+375(29)375-44-33", "IPhone XR", 250.7);
        Phone samsung = new ("+375(44)510-43-48", "Samsung S23 Ultra", 150.8);
        Phone blackberry = new ("+375(29)110-44-41", "Blackberry S34");
        Phone prestigio = new Phone (); 
        Console.WriteLine("____________________________________________________");
        Console.WriteLine(xiaomi.GetInfo());
        Console.WriteLine(iphone.GetInfo());
        Console.WriteLine(samsung.GetInfo());
        Console.WriteLine(blackberry.GetInfo());
        Console.WriteLine(prestigio.GetInfo());
        Console.WriteLine("____________________________________________________");
        Console.WriteLine(xiaomi.getNumber());
        Console.WriteLine(iphone.getNumber());
        Console.WriteLine(samsung.getNumber());
        Console.WriteLine(blackberry.getNumber());
        Console.WriteLine("____________________________________________________");
        Console.WriteLine(xiaomi.recieveCall("Pavel"));
        Console.WriteLine(xiaomi.recieveCall("Pavel", "+375(44)510-43-48"));
        Console.WriteLine(iphone.recieveCall("Eugeniy"));
        Console.WriteLine(iphone.recieveCall("Eugeniy", "+375(29)375-44-33"));
        Console.WriteLine(samsung.recieveCall("Andrey"));
        Console.WriteLine(samsung.recieveCall("Andrey", "+375(29)111-22-33"));
        Console.WriteLine(xiaomi.recieveCall("Andrey", "+375(29)111-22-33"));
        Console.WriteLine("____________________________________________________");
        Console.WriteLine(xiaomi.sendMessage("+375(29)444-22-33", "+375(29)111-22-33", "+375(25)555-11-99"));
        Console.WriteLine("____________________________________________________");
        CreditCard firstCard = new CreditCard ("12345678", 100.0);
        CreditCard secondCard = new CreditCard("87654321", 100.0);
        CreditCard thirdCard = new CreditCard("12348765", 100.0);
        Console.WriteLine($"{firstCard.GetInfo()}, {secondCard.GetInfo()}, {thirdCard.GetInfo()}");
        firstCard.increaseBalace(125);
        secondCard.increaseBalace(144);
        thirdCard.decreaseBalace(99);
        Console.WriteLine($"{firstCard.GetInfo()}, {secondCard.GetInfo()}, {thirdCard.GetInfo()}");
    }
}
