﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson6.Models
{
    internal class Phone
    {
        private string? number = "Unknown";
        private string? model = "Untitled";
        private double weight;

        public Phone() 
        {
            this.number = "Unknown";
            this.model = "Untitled";
            this.weight = 0.0;
        }

        public Phone(string number, string model, double weight)
        {
            this.number = number;
            this.model = model; 
            this.weight = weight;
        }
        public Phone(string number, string model): this(number, model, 18)
        {
        }
        public string recieveCall (string name)
        {
            return $"({model}) Звонит: {name}";
        }
        public string recieveCall (string name, string number)
        {
            return $"({model}) Звонит: {name} Номер: {number}";
        }
        public string GetInfo()
        {
            return $"Number: {number}, Model: {model}, Weight: {weight}";
        }
        public string getNumber()
        {
            return $"Number: {number}";
        }
        public string sendMessage(string num1, string num2, string num3)
        {
            return $"Numbers to send messages: {num1}, {num2},{num3}";
        }



    }
}
