﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson6.Models
{
    internal class CreditCard
    {
        private string? AccountNumber;
        private double AccountBalance;

        public CreditCard()
        {
            this.AccountNumber = "Unknown";
            this.AccountBalance = 0.0;
        }
        public CreditCard(string AccountNumber, double AccountBalance)
        {
            this.AccountNumber = AccountNumber;
            this.AccountBalance = AccountBalance;
        }
        public string GetInfo()
        {
            return $"{AccountNumber}: {AccountBalance}";
        }
        public void increaseBalace(double a)
        {
           this.AccountBalance += a;
        }
        public void decreaseBalace(double a)
        {
            if (this.AccountBalance > a)
                this.AccountBalance -= a;
            else this.AccountBalance = this.AccountBalance;
        }

    }
}
